# ss-lbs-f19-group3

# Deploying A Mobile Device Management Solution

## Team
Samuel Akpan (akpans1), Abdul Durrani(durrani1)

## Summary of Background: 

Since the 2000s, we have seen a glaring increase in the adoption of smart mobile devices into enterprise environments. Moreso, employees are more inclined to use devices that they are familiar with, presenting a whole new field of security concerns for companies. Since these devices host confidential corporate information it has become necessary to implement solutions that address these concerns. In this project we will be demonstrating the capabilities of a mobile device management solution as applicable in corporate environments across all industry categories.

## Project Objectives:
To obtain a suitable mobile device management solution for mobile device security concerns.
To implement security policies for corporately used mobile devices. 
To demonstrate the effectiveness of mobile device management solutions.

## Expected contributions and the relevance to the course topics: 
By improving the security of applications on mobile devices, this project will address the security concerns of confidential information contained in such devices.

## Group Plan: 
The group will conduct weekly meetings to implement the technical aspects of the project as well as author the relevant and necessary documentation. Each member will have dedicated tasks to complete within a weekly deadline to ensure progress. 

## Timeline:
10/10/2019: Create Virtual Machines and Set up cloud Extender
10/14/2019: Portal Customization
10/21/2019: Design Security Policies I
10/30/2019: Design Security Policies II 
11/01/2019: Setup User Account Dashboard
10/02/2019: Enroll Devices [Android/IOS] 
10/03/2019: Testing and Deployment

## Progress Report:
So far, we have been able to set up the virtual machines with the corresponding cloud extender applications. We have created and customized our user portals for the project.
The User accounts have been set up for both of us working on this project. 
Now we are currently working on agreeing on what security policies to implement on the devices which we will manage. While this is ongoing, we have already begun testing by enrolling test devices on the management platform. 



Trello Board Link: https://trello.com/b/B4cO2uC7